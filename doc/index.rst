.. JetSeT documentation master file

.. image:: _static/logo_large.png
   :width: 600px



:Author: `Andrea Tramacere <andrea.tramacere@gmail.com>`_


This page provides the documentation for the  ``JetSeT`` package, a framework providing tools for:

* reproducing radiative and accelerative process acting in relativistic jets
* modeling and fitting multiwavelength SEDs
* handling  observed data





.. _user-docs:



Documentation
---------------------

.. toctree::
   :maxdepth: 1

   installation <install.rst>
   user guide <user_guide/user_guide.rst>
   code documentation (API) <api/modules.rst>


..   jet model <user_guide/jet_model/Jet_example.rst>
..   model fitting  <user_guide/model_fit/fit_example.rst>
..

..   introduction <intro/intro.rst>
..   tutorial <tutorial/tutorial.rst>


Acknowledgements
---------------------


If you use this code in any kind of scientific publication please cite the following papers:

* `Tramacere A. et al. 2011 <http://adsabs.harvard.edu/abs/2011ApJ...739...66T>`_
* `Tramacere A. et al. 2009 <http://adsabs.harvard.edu/abs/2009A%26A...501..879T>`_
* `Massaro E. et. al 2006 <http://adsabs.harvard.edu/abs/2006A%26A...448..861M>`_






Index
---------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

