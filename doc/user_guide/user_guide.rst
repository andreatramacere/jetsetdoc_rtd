=================
JetSeT User Guide
=================


.. toctree::
   :maxdepth: 1

   jet model: physcal setup <jet_model_phys/Jet_example_phys.rst>
   jet model: nemerical setup   <jet_model_num/Jet_example_num.rst>
   load data <load_data/Jet_example_load_data.rst>
   phenomological constrain  <model_fit/fit_example.rst>
   model fitting  <model_fit/fit_example.rst>

 

   
